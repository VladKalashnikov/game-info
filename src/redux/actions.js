import {
  SET_CURR_PAGE,
  SET_GAMES_INFO,
  SET_SPECIFIC_GAME_INFO,
  SET_TEXT_FIELD_VAL,
  SET_SEARCH_TERM,
  SET_TOTAL_RESULTS,
} from "./actionTypes";

export const setCurrPage = (page) => ({
  type: SET_CURR_PAGE,
  payload: { page },
});

export const addGamesInfo = (gamesInfo) => ({
  type: SET_GAMES_INFO,
  payload: { gamesInfo },
});

export const setTotalResults = (totalResults) => ({
  type: SET_TOTAL_RESULTS,
  payload: { totalResults },
});

export const setSpecificGameInfo = ([specificGameInfo]) => ({
  type: SET_SPECIFIC_GAME_INFO,
  payload: { specificGameInfo },
});

export const setTextFieldValue = (value) => ({
  type: SET_TEXT_FIELD_VAL,
  payload: { value },
});

export const setSearchTerm = (searchTerm) => ({
  type: SET_SEARCH_TERM,
  payload: { searchTerm },
});
