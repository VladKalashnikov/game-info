import { createStore } from "redux";
import reducer from "./reducers/gamesInfo";

export default createStore(reducer);