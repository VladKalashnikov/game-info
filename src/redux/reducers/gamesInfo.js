import {
  SET_CURR_PAGE,
  SET_GAMES_INFO,
  SET_SPECIFIC_GAME_INFO,
  SET_TEXT_FIELD_VAL,
  SET_SEARCH_TERM,
  SET_TOTAL_RESULTS,
} from "../actionTypes";

const initialState = {
  page: 1,
  gamesInfo: [],
  totalResults: 0,
  specificGameInfo: {},
  textFieldValue: "",
  searchTerm: "",
};

export default (state = initialState, { type, payload }) => {
  let newState;
  
  switch (type) {
    case SET_CURR_PAGE:
      const { page } = payload;
      newState = { ...state, page };
      return newState;

    case SET_GAMES_INFO:
      const { gamesInfo } = payload;
      newState = { ...state, gamesInfo };
      return newState;

    case SET_SPECIFIC_GAME_INFO:
      const { specificGameInfo } = payload;
      newState = { ...state, specificGameInfo };
      return newState;

    case SET_TEXT_FIELD_VAL:
      const { value } = payload;
      newState = { ...state, textFieldValue: value };
      return newState;

    case SET_SEARCH_TERM:
      const { searchTerm } = payload;
      newState = { ...state, searchTerm };
      return newState;

    case SET_TOTAL_RESULTS:
      const { totalResults } = payload;
      newState = { ...state, totalResults };
      return newState;

    default:
      return state;
  }
};
