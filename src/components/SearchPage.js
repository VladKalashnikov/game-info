import React from "react";
import GameList from "./GameList";
import SearchBar from "./SearchBar";

export default function SearchPage() {
  return (
    <React.Fragment>
      <SearchBar />
      <GameList />
    </React.Fragment>
  );
}
