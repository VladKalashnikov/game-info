import React from "react";
import { TextField, Button } from "@material-ui/core";
import {
  setCurrPage,
  setTextFieldValue,
  setSearchTerm,
  addGamesInfo,
  setTotalResults,
} from "../redux/actions";
import { connect } from "react-redux";
import { GAMES_PER_PAGE } from "../constants";
import IGDBApiClient from "../services/IGDBApiClient";

function SearchBar(props) {
  
  const handleBtnClick = () => {
    const igdbApiClient = new IGDBApiClient();
    igdbApiClient
      .findGamesCount(props.textFieldValue)
      .then(({ count }) => {
        if (count && count > 0) {
          igdbApiClient
            .findGamesWithCovers(props.textFieldValue, GAMES_PER_PAGE)
            .then((gamesInfo) => {
              props.addGamesInfo(gamesInfo);
              props.setTotalResults(count);
              props.setCurrPage(1);
              props.setSearchTerm(props.textFieldValue);
              props.setTextFieldValue("");
            });
        }
      })
      .catch((error) => console.error(error));
  };

  return (
    <div className="mt-5 mb-3">
      <TextField
        onChange={(e) => props.setTextFieldValue(e.target.value)}
        value={props.textFieldValue}
        id="outlined-basic"
        label="Search game"
        variant="outlined"
        size="small"
        className="w-75"
      />
      <Button onClick={handleBtnClick} variant="contained" size='medium' className="mx-1">
        Search
      </Button>
    </div>
  );
}

const mapStateToProps = ({ textFieldValue }) => {
  return { textFieldValue };
};

export default connect(mapStateToProps, {
  setCurrPage,
  setTextFieldValue,
  addGamesInfo,
  setSearchTerm,
  setTotalResults,
})(SearchBar);
