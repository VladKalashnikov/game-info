import React, { useEffect } from "react";
import IGDBApiClient from "../services/IGDBApiClient";
import { GAMES_PER_PAGE } from "../constants";
import { connect } from "react-redux";
import { setCurrPage, addGamesInfo, setTotalResults } from "../redux/actions";
import { Link } from "react-router-dom";
import { Pagination } from "@material-ui/lab";
import { GridList, GridListTile, GridListTileBar } from "@material-ui/core";
import img_not_found from "../images/image-not-found.jpg";

function GameList(props) {
  // Get top 10 games on component mount
  useEffect(() => {
    const igdbApiClient = new IGDBApiClient();
    igdbApiClient
      .getTopGamesWithCovers(GAMES_PER_PAGE)
      .then((gamesInfo) => {
        props.addGamesInfo(gamesInfo);
        props.setTotalResults(GAMES_PER_PAGE);
      })
      .catch((error) => console.error(error));
  }, []);

  const handlePageChange = (e, page) => {
    const { searchTerm } = props;
    const offset = (page - 1) * GAMES_PER_PAGE;
    const igdbApiClient = new IGDBApiClient();
    igdbApiClient
      .findGamesWithCovers(searchTerm, GAMES_PER_PAGE, offset)
      .then((gamesInfo) => {
        props.addGamesInfo(gamesInfo);
        props.setCurrPage(page);
      })
      .catch((error) => console.error(error));
  };

  const gameInfoExist = props.gamesInfo && props.gamesInfo.length > 0;

  let listItems = <li>Loading...</li>;

  if (gameInfoExist) {
    const gridTiles = props.gamesInfo.map((e) => {
      return (
        <GridListTile key={e.id} className="h-auto">
          <Link to={`/game/${e.id}`} className="text-center">
            <div>
              <img
                src={e.coverUrl || img_not_found}
                alt={e.name}
                className="img-fluid align-middle"
              />
              <GridListTileBar title={e.name} />
            </div>
          </Link>
        </GridListTile>
      );
    });

    listItems = <GridList cols={2}>{gridTiles}</GridList>;
  }

  const pagesCount = props.gamesInfo
    ? Math.ceil(props.totalResults / GAMES_PER_PAGE)
    : 1;

  return (
    <div>
      <h2>Top 10 games or search result</h2>
      {listItems}
      {pagesCount > 1 && (
        <Pagination
          page={props.page}
          count={pagesCount}
          color="primary"
          onChange={handlePageChange}
          size="large"
          className="mx-auto my-3 w-50"
        />
      )}
    </div>
  );
}

function mapStateToProps({ page, gamesInfo, searchTerm, totalResults }) {
  return { page, gamesInfo, searchTerm, totalResults };
}

export default connect(mapStateToProps, {
  setCurrPage,
  addGamesInfo,
  setTotalResults,
})(GameList);
