import React, { useEffect } from "react";
import IGDBApiClient from "../services/IGDBApiClient";
import { useParams } from "react-router-dom";
import { setSpecificGameInfo } from "../redux/actions";
import { connect } from "react-redux";
import {
  Container,
  Table,
  TableContainer,
  TableBody,
  TableRow,
  TableCell,
  Paper,
} from "@material-ui/core";

function GamePage(props) {
  const { id } = useParams();

  useEffect(() => {
    const igdbApiClient = new IGDBApiClient();
    igdbApiClient
      .findGameById(id)
      .then((gameInfo) => props.setSpecificGameInfo(gameInfo))
      .catch((error) => console.error(error));
  });

  let { specificGameInfo: info } = props;
  let renderedContent = <p>Loading...</p>;

  if (info) {
    const rows = [
      { key: "Name", value: info.name },
      { key: "Rating", value: info.rating },
      { key: "Url", value: info.url },
      { key: "Summary", value: info.summary },
    ];

    renderedContent = (
      <React.Fragment>
        <h1 className="mt-3">{info.name}</h1>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.key}>
                  <TableCell component="th" scope="row">
                    {row.key}
                  </TableCell>
                  <TableCell>{row.value}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </React.Fragment>
    );
  }

  return <Container maxWidth="sm">{renderedContent}</Container>;
}

function mapStateToProps({ specificGameInfo }) {
  return { specificGameInfo };
}

export default connect(mapStateToProps, { setSpecificGameInfo })(GamePage);
