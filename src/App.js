import React from "react";
import { Switch, Route } from "react-router-dom";
import SearchPage from "./components/SearchPage";
import GamePage from "./components/GamePage";
import { Container } from "@material-ui/core";

function App() {
  return (
    <React.Fragment>
      <h1 className="mt-2 border border-dark text-center">Game Info</h1>
      <Container maxWidth="md" className="text-center">
        <Switch>
          <Route path="/" exact>
            <SearchPage />
          </Route>
          <Route path="/game/:id" exact>
            <GamePage />
          </Route>
        </Switch>
      </Container>
    </React.Fragment>
  );
}

export default App;
