class IGDBApiClient {
  constructor() {
    this.corsApiUrl = "https://cors-anywhere.herokuapp.com";
    this.igdbApiBase = "https://api.igdb.com/v4";
    this.apiBase = `${this.corsApiUrl}/${this.igdbApiBase}`;
    this.gameEndpoint = "games";
    this.coversEndpoint = "covers";
    this.defaultHeader = {
      "Content-Type": "text/plain",
      Accept: "application/json",
      "user-key": process.env.REACT_APP_IDGB_API_KEY || "",
      headers: {
        "x-requested-with": "xhr",
      },
    };
  }

  async findGamesCount(searchTerm) {
    try {
      const response = await fetch(
        `${this.apiBase}/${this.gameEndpoint}/count`,
        {
          method: "POST",
          headers: this.defaultHeader,
          body: `search "${searchTerm}";`,
        }
      );

      return await this.handleResponse(response);
    } catch (error) {
      console.error(error);
    }
  }

  async findGames(searchTerm, amount = 0, offset = 0) {
    try {
      const response = await fetch(`${this.apiBase}/${this.gameEndpoint}`, {
        method: "POST",
        headers: this.defaultHeader,
        body: `search "${searchTerm}"; fields id,name; limit ${amount}; offset ${offset};`,
      });

      return await this.handleResponse(response);
    } catch (error) {
      console.error(error);
    }
  }

  async findGamesWithCovers(searchTerm, amount = 0, offset = 0) {
    try {
      const gamesInfo = await this.findGames(searchTerm, amount, offset)
      const gamesIds = gamesInfo.map((e) => e.id);
      const covers = await this.getGameCovers(gamesIds);

      return this.combineGamesWithCovers(gamesInfo, covers);
    } catch (error) {
      console.error(error);
    }
  }

  async findGameById(id) {
    try {
      const response = await fetch(`${this.apiBase}/${this.gameEndpoint}`, {
        method: "POST",
        headers: this.defaultHeader,
        body: `fields id,name,rating,url,summary; where id = ${id};`,
      });

      return await this.handleResponse(response);
    } catch (error) {
      console.error(error);
    }
  }

  async getTopGames(amountToGet = 10) {
    try {
      const response = await fetch(`${this.apiBase}/${this.gameEndpoint}`, {
        method: "POST",
        headers: this.defaultHeader,
        body: `fields id,name; sort popularity desc; limit ${amountToGet};`,
      });

      return await this.handleResponse(response);

    } catch (error) {
      console.error(error);
    }
  }

  async getTopGamesWithCovers(amountToGet = 10) {
    try {
      const gamesInfo = await this.getTopGames(amountToGet);
      const gamesIds = gamesInfo.map((e) => e.id);
      const covers = await this.getGameCovers(gamesIds);

      return this.combineGamesWithCovers(gamesInfo, covers);

    } catch (error) {
      console.error(error);
    }
  }

  async getGameCovers(...gameIds) {
    try {
      const response = await fetch(`${this.apiBase}/${this.coversEndpoint}`, {
        method: "POST",
        headers: this.defaultHeader,
        body: `fields image_id,game; where game = (${gameIds.join()});`,
      });

      const coversInfo = await this.handleResponse(response);
      return coversInfo.map((e) => ({
        gameId: e.game,
        coverUrl: `https://images.igdb.com/igdb/image/upload/t_720p/${e.image_id}.jpg`,
      }));
      
    } catch (error) {
      console.error(error);
    }
  }

  async handleResponse(response) {
    if (!response) {
      throw new Error("'response' parameter missing");
    }

    const json = await response.json();

    if (!response.ok) {
      throw new Error(
        "Response is not successful." +
          `Status code: ${response.status} ${response.statusText}.` +
          `\nResponse body:\n${JSON.stringify(json)}`
      );
    }

    return json;
  }

  combineGamesWithCovers(gamesInfo, covers) {
    return gamesInfo.map((e) => {
      // Assigning cover urls to game info objects.
      const coverInfo = covers.find((c) => c.gameId === e.id);
      if (coverInfo != undefined) {
        e.coverUrl = coverInfo.coverUrl;
      }
      return e;
    });
  }
}

export default new IGDBApiClient();
